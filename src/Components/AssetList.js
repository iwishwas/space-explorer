import React from 'react';
import PropTypes from 'prop-types';

const AssetList = ({
  assets,
  userList,
  formatConversion,
  handleSort,
  assetSort,
  sortSwapClass,
}) => (
  assets.length > 0
    ? (
      <table className="table table-striped table-hover table-bordered">
        <tbody>
          <tr>
            {
              Object.keys(assets[0]).filter(key => key !== 'id' && key !== 'type').map(key => (
                <th className="link" key={key}>
                  <i
                    className={sortSwapClass(assetSort, key)}
                    onClick={() => handleSort(key, assets[0].type)}
                  />
                  {key.toUpperCase()}
                </th>))
            }
          </tr>
          {
            assets.map((asset, index) => (
              <tr key={asset.id} className={`assest ${index}`}>
                <td>
                  {asset.title}
                </td>
                <td>
                  {asset.contentType}
                </td>
                <td>
                  {asset.fileName}
                </td>
                <td>
                  {userList ? userList[asset.createdBy] : ''}
                </td>
                <td>
                  {userList ? userList[asset.updatedBy] : ''}
                </td>
                <td>
                  {formatConversion(asset.updatedAt)}
                </td>
              </tr>
            ))
          }
        </tbody>
      </table>
    )
    : (<h2>Not Available</h2>)
);

AssetList.propTypes = {
  assets: PropTypes.arrayOf(PropTypes.shape({
    contentType: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    fileName: PropTypes.string.isRequired,
    createdBy: PropTypes.string.isRequired,
    updatedBy: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
  })).isRequired,
  userList: PropTypes.objectOf(PropTypes.string).isRequired,
  formatConversion: PropTypes.func.isRequired,
  handleSort: PropTypes.func.isRequired,
  assetSort: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  sortSwapClass: PropTypes.func.isRequired,
};


export default AssetList;
