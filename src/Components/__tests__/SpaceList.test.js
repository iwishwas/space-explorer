import React from 'react';
import renderer from 'react-test-renderer';
import SpaceList from '../SpaceList';

const spaces = [{ id: '1', title: 'Simple title' }]
const spaceListClick = jest.fn();

test('Testing SpaceList with manual initialisation!', () => {
  const component = renderer.create(<SpaceList
    spaces={spaces}
    spaceListClick={spaceListClick}
  />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
