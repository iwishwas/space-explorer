import React from 'react';
import renderer from 'react-test-renderer';
import { mount, configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from '../App';
import api from '../../api';

configure({ adapter: new Adapter() });

const spaces = {
  sys: {
    type: 'Array',
  },
  total: 3,
  skip: 0,
  limit: 100,
  items: [{
    fields: {
      title: 'My Second Space',
      description: '',
    },
    sys: {
      id: 'yadj1kx9rmg01',
      type: 'Space',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-05-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU',
    },
  }],
};

const users = {
  sys: {
    type: 'Array',
  },
  total: 1,
  skip: 0,
  limit: 100,
  items: [
    {
      fields: {
        name: 'Alana Atlassian',
        role: 'Author',
      },
      sys: {
        id: '4FLrUHftHW3v2BLi9fzfjU',
        type: 'User',
      },
    },
    {
      fields: {
        name: 'John Doe',
        role: 'Editor',
      },
      sys: {
        id: '4FLrUHftHW3v2BLi9fzfjU2',
        type: 'User',
      },
    },
  ],
};
const entries = {
  sys: {
    type: 'Array',
  },
  total: 3,
  skip: 0,
  limit: 100,
  items: [{
    fields: {
      title: 'Hero Collaboration Partial',
      contentType: 'image/png',
      fileName: 'hero-collaboration-partial.png',
      upload: 'https://wac-cdn.atlassian.com/dam/jcr:51be4df5-1ffb-4a4d-9f44-0b84dad9de5e/hero-collaboration-partial.png',
    },
    sys: {
      id: 'wtrHxeu3zEoEce2MokCSi1',
      type: 'Asset',
      version: 1,
      space: 'yadj1kx9rmg0',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-05-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU',
    },
  }, {
    fields: {
      title: 'Stride Chat',
      contentType: 'image/png',
      fileName: 'stride_chat.svg',
      upload: 'https://wac-cdn.atlassian.com/dam/jcr:61741d76-b9a0-44b1-a31f-bfde8e6930ab/stride_chat.svg',
    },
    sys: {
      id: 'wtrHxeu3zEoEce2MokCSi2',
      type: 'Asset',
      version: 1,
      space: 'yadj1kx9rmg0',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-05-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU2',
    },
  }],
};
const assets = {
  sys: {
    type: 'Array',
  },
  total: 3,
  skip: 0,
  limit: 100,
  items: [{
    fields: {
      title: 'Hello, World!',
      summary: 'A Hello World is a common starting point for learning a new language or platform.',
      body: 'YYYYYYYY',
    },
    sys: {
      id: '5KsDBWseXY6QegucYAoacS1',
      type: 'Entry',
      version: 1,
      space: 'yadj1kx9rmg0',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-05-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU',
    },
  },
  {
    fields: {
      title: 'Tutorial 1',
      summary: 'In this tutorial, we will dive deeper into how to leverage the new platform.',
      body: 'XXXXXX',
    },
    sys: {
      id: '5KsDBWseXY6QegucYAoacS2',
      type: 'Entry',
      version: 1,
      space: 'yadj1kx9rmg0',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-12-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU2',
    },
  }],
};

api.fetchUsers = jest.fn(() => Promise.resolve(users));
api.fetchSpaces = jest.fn(() => Promise.resolve(spaces));
api.fetchSpaceEntries = jest.fn(() => Promise.resolve(entries));
api.fetchSpaceAssets = jest.fn(() => Promise.resolve(assets));

describe('spyOn', () => {
  let didMountSpy; // Reusing the spy, and clear it with mockClear()
  afterEach(() => {
    didMountSpy.mockClear();
  });

  didMountSpy = jest.spyOn(App.prototype, 'componentDidMount');

  test('should fetch', () => {
    // Ensure the componentDidMount haven't called yet.
    expect(didMountSpy).toHaveBeenCalledTimes(0);

    const TFCRender = mount(<App />);
    expect(didMountSpy).toHaveBeenCalledTimes(1);
    expect(api.fetchUsers).toHaveBeenCalledTimes(1);
    expect(api.fetchSpaces).toHaveBeenCalledTimes(1);
    expect(api.fetchSpaceEntries).toHaveBeenCalledTimes(0);
    expect(api.fetchSpaceAssets).toHaveBeenCalledTimes(0);
  });

});


describe('App Component (Snapshot)', () => {
  it('App component rendering...', () => {
    const component = renderer.create(<App />);
    const json = component.toJSON();
    expect(json).toMatchSnapshot();
  });
});

describe('Sort ', () => {
  it('Sorting...', () => {
    const wrap = shallow(
      <App />,
    );
    wrap.setState({
      currentSpace: {
        createdby: '4FLrUHftHW3v2BLi9fzfjU',
        description: 'This space was created by Alanna Atlassian as an example of how to create a space and relate assets and entries to it.',
        id: 'yadj1kx9rmg0',
        title: 'My First Space',
      },
      entries: [{
        id: '123',
        title: 'Ramesh',
        type: 'Entry',
        summary: 'Summary',
        createdBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedAt: '2015-05-18T11:29:46.809Z',
      }, {
        id: '123',
        title: 'Help',
        type: 'Entry',
        summary: 'Summary',
        createdBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedAt: '2015-05-18T11:29:46.809Z',
      }, {
        id: '123',
        title: 'Vishwas',
        type: 'Entry',
        summary: 'Summary',
        createdBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedAt: '2015-05-18T11:29:46.809Z',
      }],
      assets: [{
        id: '123',
        title: 'Zebra',
        type: 'Asset',
        contentType: 'image/png',
        fileName: 'test.png',
        createdBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedAt: '2015-05-18T11:29:46.809Z',
      }, {
        id: '123',
        title: 'Title',
        type: 'Asset',
        contentType: 'image/png',
        fileName: 'test.png',
        createdBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedAt: '2015-05-18T11:29:46.809Z',
      }, {
        id: '123',
        title: 'At pubs',
        type: 'Asset',
        contentType: 'image/png',
        fileName: 'test.png',
        createdBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedBy: '4FLrUHftHW3v2BLi9fzfjU',
        updatedAt: '2015-05-18T11:29:46.809Z',
      }],
      assetColumnToSort: 'title',
      assetSortDirection: 'desc',
      entryColumnToSort: 'title',
      entrySortDirection: 'asc',
      users: {
        '4FLrUHftHW3v2BLi9fzfjU2' : 'John Doe'
      }
    });
    expect(wrap).toMatchSnapshot()
  });
});
