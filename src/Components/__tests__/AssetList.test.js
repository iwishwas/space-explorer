import React from 'react';
import renderer from 'react-test-renderer';
import AssetList from '../AssetList';

const assets = [{
  id: '1',
  title: 'entry title',
  type: 'Entry',
  contentType: 'image/png',
  fileName: 'stride_chat.svg',
  createdBy: '4FLrUHftHW3v2BLi9fzfjU',
  updatedBy: '4FLrUHftHW3v2BLi9fzfjU2',
  updatedAt: '2015-05-18T11:29:46.809Z',
}];
const userList = {
  '4FLrUHftHW3v2BLi9fzfjU': 'Alana Atlassian',
  '4FLrUHftHW3v2BLi9fzfjU2': 'John Doe',
};
const formatConversion = (dateFormat) => {
  const date = new Date(dateFormat);
  return `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`;
};
const handleSort = jest.fn();
const sortSwapClass = (entry, key) => {
  if (entry[0] === key) {
    if (entry[1] === 'asc') return 'fa fa-fw fa-sort-asc';
    if (entry[1] === 'desc') return 'fa fa-fw fa-sort-desc';
  }
  return 'fa fa-fw fa-sort';
};
const assetSort = ['title', 'asc'];

test('Testing AssetList with manual initialisation!', () => {
  const component = renderer.create(<AssetList
    assets={assets}
    userList={userList}
    formatConversion={formatConversion}
    handleSort={handleSort}
    sortSwapClass={sortSwapClass}
    assetSort={assetSort}
  />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
