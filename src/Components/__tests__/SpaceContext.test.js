import React from 'react';
import renderer from 'react-test-renderer';
import SpaceContext from '../SpaceContext';

const entries = [{
  id: '1',
  title: 'entry title',
  type: 'Entry',
  summary: 'This is sample summary',
  createdBy: '4FLrUHftHW3v2BLi9fzfjU',
  updatedBy: '4FLrUHftHW3v2BLi9fzfjU2',
  updatedAt: '2015-05-18T11:29:46.809Z',
}];
const assets = [{
  id: '1',
  title: 'entry title',
  type: 'Entry',
  contentType: 'image/png',
  fileName: 'stride_chat.svg',
  createdBy: '4FLrUHftHW3v2BLi9fzfjU',
  updatedBy: '4FLrUHftHW3v2BLi9fzfjU2',
  updatedAt: '2015-05-18T11:29:46.809Z',
}];
const userList = {
  '4FLrUHftHW3v2BLi9fzfjU': 'Alana Atlassian',
  '4FLrUHftHW3v2BLi9fzfjU2': 'John Doe',
};
const currentSpace = {
  id: '1',
  title: 'Current Space',
  description: 'Main Space',
  createdby: '4FLrUHftHW3v2BLi9fzfjU',
};
const handleSort = jest.fn();
let sortArray = ['title', 'asc'];

test('Testing SpaceContext with manual initialisation with ascending!', () => {
  const component = renderer.create(<SpaceContext
    currentSpace={currentSpace}
    userList={userList}
    entryList={entries}
    assetList={assets}
    handleSort={handleSort}
    assetSort={sortArray}
    entrySort={sortArray}
  />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

sortArray = ['title', 'desc'];

test('Testing SpaceContext with manual initialisation with descending!', () => {
  const component = renderer.create(<SpaceContext
    currentSpace={currentSpace}
    userList={userList}
    entryList={entries}
    assetList={assets}
    handleSort={handleSort}
    assetSort={sortArray}
    entrySort={sortArray}
  />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
