import React from 'react';
import PropTypes from 'prop-types';

const SpaceList = ({
  spaces,
  spaceListClick,
}) => (
  <div className="SpaceList col-xs-6 col-md-2 text-center" style={{ marginTop: '5%' }}>
    {
      spaces.map(
        space => (
          <button
            type="button"
            key={space.id}
            className="btn btn-primary btn-lg btn-block"
            onClick={() => spaceListClick(space.id)}
          >
            {space.title}
          </button>
        ),
      )
    }
  </div>
);

SpaceList.propTypes = {
  spaces: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
    }),
  ).isRequired,
  spaceListClick: PropTypes.func.isRequired,
};

export default SpaceList;
