import React from 'react';
import PropTypes from 'prop-types';

const EntryList = ({
  entries,
  userList,
  formatConversion,
  handleSort,
  entrySort,
  sortSwapClass,
}) => (
  entries.length > 0
    ? (
      <table className="table table-striped table-hover table-bordered">
        <tbody>
          <tr>
            {
              Object.keys(entries[0]).filter(key => key !== 'id' && key !== 'type').map(key => (
                <th key={key} className="link">
                  <i className={sortSwapClass(entrySort, key)}
                    onClick={() => handleSort(key, entries[0].type)}
                  />
                  {key.toUpperCase()}
                </th>))
            }
          </tr>
          {
            entries.map((entry, index) => (
              <tr key={entry.id} className={`entry ${index}`}>
                <td>
                  {entry.title}
                </td>
                <td>
                  {entry.summary}
                </td>
                <td>
                  {userList ? userList[entry.createdBy] : ''}
                </td>
                <td>
                  {userList ? userList[entry.updatedBy] : ''}
                </td>
                <td>
                  {formatConversion(entry.updatedAt)}
                </td>
              </tr>
            ))
          }
        </tbody>
      </table>
    )
    : (<h2>Not Available</h2>)
);

EntryList.propTypes = {
  entries: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    createdBy: PropTypes.string.isRequired,
    updatedBy: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
  })).isRequired,
  userList: PropTypes.objectOf(PropTypes.string).isRequired,
  formatConversion: PropTypes.func.isRequired,
  handleSort: PropTypes.func.isRequired,
  entrySort: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  sortSwapClass: PropTypes.func.isRequired,
};

export default EntryList;
