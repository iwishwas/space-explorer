import React from 'react';
import PropTypes from 'prop-types';
import EntryList from './EntryList';
import AssetList from './AssetList';

const dateConversion = (dateFormat) => {
  const date = new Date(dateFormat);
  return `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`;
};

const sortSwapClass = (entry, key) => {
  if (entry[0] === key) {
    if (entry[1] === 'asc') return 'fa fa-fw fa-sort-asc';
    if (entry[1] === 'desc') return 'fa fa-fw fa-sort-desc';
  }
  return 'fa fa-fw fa-sort';
};

const SpaceContext = ({
  currentSpace,
  userList,
  entryList,
  assetList,
  handleSort,
  assetSort,
  entrySort,
}) => (
  <div className="SpaceContext col-xs-12 col-sm-6 col-md-10">
    {
      currentSpace ? (
        <div style={{ marginTop: '3%', marginLeft: '2%' }}>
          <div className="header">
            <h1>
              {currentSpace.title}
              <small>
                {` created by ${userList[currentSpace.createdby]}`}
              </small>
            </h1>
          </div>
          <div className="description" style={{ marginTop: '3%' }}>
            <textarea className="form-control" rows="5" id="formGroupInputLarge" placeholder={currentSpace.description.length === 0 ? 'Not Available' : currentSpace.description} disabled style={{ resize: 'none', width: '50%' }} />
          </div>
          <div className="entries-assets-modal table-responsive" style={{ marginTop: '3%' }}>
            <ul className="nav nav-tabs">
              <li className="active"><a data-toggle="tab" href="#entries">Entries</a></li>
              <li><a data-toggle="tab" href="#assets">Assets</a></li>
            </ul>
            <div className="tab-content">
              <div id="entries" className="tab-pane fade in active">
                <div className="entries-table">
                  <EntryList
                    userList={userList}
                    entries={entryList}
                    handleSort={handleSort}
                    formatConversion={dateConversion}
                    entrySort={entrySort}
                    sortSwapClass={sortSwapClass}
                  />
                </div>
              </div>
              <div id="assets" className="tab-pane fade">
                <div className="assets-table">
                  <AssetList
                    userList={userList}
                    assets={assetList}
                    handleSort={handleSort}
                    formatConversion={dateConversion}
                    assetSort={assetSort}
                    sortSwapClass={sortSwapClass}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : ''
    }
  </div>
);

SpaceContext.propTypes = {
  entryList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    createdBy: PropTypes.string.isRequired,
    updatedBy: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
  })).isRequired,
  assetList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    fileName: PropTypes.string.isRequired,
    contentType: PropTypes.string.isRequired,
    createdBy: PropTypes.string.isRequired,
    updatedBy: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
  })).isRequired,
  userList: PropTypes.objectOf(PropTypes.string).isRequired,
  handleSort: PropTypes.func.isRequired,
  currentSpace: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    createdby: PropTypes.string,
  }),
  assetSort: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  entrySort: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
};

SpaceContext.defaultProps = {
  currentSpace: null,
};

export default SpaceContext;
