import React, { Component } from 'react';
import SpaceList from './SpaceList';
import SpaceContext from './SpaceContext';
import api from '../api';

const pushState = (obj, url) => window.history.pushState(obj, '', url);

const onPopState = (handler) => {
  window.onpopstate = handler;
  return window.onpopstate;
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSpace: null,
      spaces: [],
      users: {},
      entries: [],
      assets: [],
      assetColumnToSort: '',
      entryColumnToSort: '',
      assetSortDirection: 'desc',
      entrySortDirection: 'desc',
    };
    this.invertDirection = {
      asc: 'desc',
      desc: 'asc',
    };
    this.spaceListOnClick = this.spaceListOnClick.bind(this);
    this.handleSort = this.handleSort.bind(this);
  }

  componentDidMount() {
    Promise.all([
      api.fetchUsers(),
      api.fetchSpaces(),
    ]).then((res) => {
      const users = res[0];
      window.localStorage.setItem('users', JSON.stringify(users));
      const spaces = res[1];
      const currentSpace = spaces[0];
      window.localStorage.setItem('currentSpace', JSON.stringify(currentSpace));
      window.localStorage.setItem('spaces', JSON.stringify(spaces));
      Promise.all([
        api.fetchSpaceEntries(currentSpace.id),
        api.fetchSpaceAssets(currentSpace.id)]).then((resp) => {
        const entries = resp[0];
        window.localStorage.setItem(`entries ${currentSpace.id}`, JSON.stringify(entries));
        const assets = resp[1];
        window.localStorage.setItem(`assets ${currentSpace.id}`, JSON.stringify(assets));
        this.setState({
          users,
          currentSpace,
          spaces,
          entries,
          assets,
        });
      }).catch(() => {
        if (window.localStorage.getItem(`entries ${currentSpace.id}`) && window.localStorage.getItem(`assets ${currentSpace.id}`)) {
          this.setState({
            assets: JSON.parse(window.localStorage.getItem(`assets ${currentSpace.id}`)),
            entries: JSON.parse(window.localStorage.getItem(`entries ${currentSpace.id}`)),
          });
        } else {
          // throw an exception
          this.setState({
            assets: [],
            entries: [],
          });
        }
      });
    }).catch(() => {
      if (window.localStorage.getItem('users') && window.localStorage.getItem('currentSpace') && window.localStorage.getItem('spaces')) {
        this.setState({
          users: JSON.parse(window.localStorage.getItem('users')),
          currentSpace: JSON.parse(window.localStorage.getItem('currentSpace')),
          spaces: JSON.parse(window.localStorage.getItem('spaces')),
        });
      } else {
        // throw an exception
        this.setState({
          users: {},
          currentSpace: null,
          spaces: [],
        });
      }
    });

    onPopState((event) => {
      const { spaces } = this.state;
      this.setState({
        currentSpace: spaces.find(element => element.id === (event.state || {}).currentSpace.id),
      });
    });
  }

  componentWillUnmount() {
    onPopState(null);
  }

  sortEntriesAssets(arr, key, dir) {
    if (arr.length === 0) return arr;
    const { users } = this.state;

    switch (dir) {
      case 'asc':
        arr.sort((a, b) => {
          if (key === 'createdBy' || key === 'updatedBy') {
            if (users[a[key]] < users[b[key]]) return -1;
            if (users[a[key]] > users[b[key]]) return 1;
          } else if (key === 'updatedAt') {
            if (new Date(a[key]) < new Date(b[key])) return -1;
            if (new Date(a[key]) > new Date(b[key])) return 1;
          } else {
            if (a[key] < b[key]) return -1;
            if (a[key] > b[key]) return 1;
          }
          return 0;
        });
        break;
      case 'desc':
        arr.sort((a, b) => {
          if (key === 'createdBy' || key === 'updatedBy') {
            if (users[a[key]] > users[b[key]]) return -1;
            if (users[a[key]] < users[b[key]]) return 1;
          } else if (key === 'updatedAt') {
            if (new Date(a[key]) > new Date(b[key])) return -1;
            if (new Date(a[key]) < new Date(b[key])) return 1;
          } else {
            if (a[key] > b[key]) return -1;
            if (a[key] < b[key]) return 1;
          }
          return 0;
        });
        break;
      default:
        break;
    }
    return arr;
  }

  fetchEntries(spaceID) {
    api.fetchSpaceEntries(spaceID).then((entries) => {
      window.localStorage.setItem(`entries ${spaceID}`, JSON.stringify(entries));
      this.setState(
        {
          entries,
        },
      );
    }).catch(() => {
      if (window.localStorage.getItem(`assets ${spaceID}`)) {
        this.setState(
          {
            entries: JSON.parse(window.localStorage.getItem(`entries ${spaceID}`)),
          },
        );
      } else {
        // throw an exception
        this.setState(
          {
            entries: [],
          },
        );
      }
    });
  }

  fetchAssets(spaceID) {
    api.fetchSpaceAssets(spaceID).then((assets) => {
      window.localStorage.setItem(`assets ${spaceID}`, JSON.stringify(assets));
      this.setState(
        {
          assets,
        },
      );
    }).catch(() => {
      if (window.localStorage.getItem(`assets ${spaceID}`)) {
        this.setState(
          {
            assets: JSON.parse(window.localStorage.getItem(`assets ${spaceID}`)),
          },
        );
      } else {
        // throw an exception
        this.setState(
          {
            assets: [],
          },
        );
      }
    });
  }

  handleSort(key, type) {
    const {
      entryColumnToSort,
      assetColumnToSort,
      assetSortDirection,
      entrySortDirection,
    } = this.state;
    if (type === 'Asset') {
      this.setState({
        assetColumnToSort: key,
        assetSortDirection: assetColumnToSort === key ? this.invertDirection[assetSortDirection] : 'asc',
      });
    }
    if (type === 'Entry') {
      this.setState({
        entryColumnToSort: key,
        entrySortDirection: entryColumnToSort === key ? this.invertDirection[entrySortDirection] : 'asc',
      });
    }
  }

  spaceListOnClick(spaceID) {
    const { spaces } = this.state;
    pushState({
      currentSpace: spaceID,
    }, `/spacexplorer/${spaceID}`);
    const space = spaces.find(element => element.id === spaceID);
    if (space) {
      this.setState({ currentSpace: space });
      this.fetchEntries(space.id);
      this.fetchAssets(space.id);
    }
  }

  render() {
    const {
      spaces,
      currentSpace,
      users,
      entries,
      assets,
      assetColumnToSort,
      entryColumnToSort,
      assetSortDirection,
      entrySortDirection,
    } = this.state;
    return (
      <div className="App container-fluid">
        {
          spaces ? (
            <div className="row-fluid">
              <SpaceList
                spaces={spaces}
                spaceListClick={this.spaceListOnClick}
              />
              <SpaceContext
                currentSpace={currentSpace}
                userList={users}
                handleSort={this.handleSort}
                assetList={this.sortEntriesAssets(assets, assetColumnToSort, assetSortDirection)}
                entryList={this.sortEntriesAssets(entries, entryColumnToSort, entrySortDirection)}
                assetSort={[assetColumnToSort, assetSortDirection]}
                entrySort={[entryColumnToSort, entrySortDirection]}
              />
            </div>
          ) : (
            <h1>Spaces array is missing</h1>
          )
          }
      </div>
    );
  }
}

export default App;
