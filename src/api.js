/* Not using fetchSpace,
 fetchSpaceEntry,
 fetchSpaceAsset,
 fetchUser */

import axios from 'axios';

const fetchSpaceEntries = spaceID => axios.get(`/space/${spaceID}/entries`).then((res) => {
  if (res.status === 200) {
    if (res.data.items[0].sys.space === spaceID) {
      return res.data.items.map(entry => ({
        id: entry.sys.id,
        title: entry.fields.title,
        type: entry.sys.type,
        summary: entry.fields.summary,
        createdBy: entry.sys.createdBy,
        updatedBy: entry.sys.updatedBy,
        updatedAt: entry.sys.updatedAt,
      }));
    }
    return [];
  }
  return [];
});

const fetchSpaceAssets = spaceID => axios.get(`/space/${spaceID}/assets`).then((res) => {
  if (res.status === 200) {
    if (res.data.items[0].sys.space === spaceID) {
      return res.data.items.map(asset => ({
        id: asset.sys.id,
        title: asset.fields.title,
        type: asset.sys.type,
        contentType: asset.fields.contentType,
        fileName: asset.fields.fileName,
        createdBy: asset.sys.createdBy,
        updatedBy: asset.sys.updatedBy,
        updatedAt: asset.sys.updatedAt,
      }));
    }
    return [];
  }
  return [];
});

const fetchUsers = () => axios.get('/users').then((res) => {
  if (res.status === 200) {
    return res.data.items.reduce((item, cur) => {
      item[cur.sys.id] = cur.fields.name;
      return item;
    }, {});
  }
  return [];
});

const fetchSpaces = () => axios.get('/space').then((res) => {
  if (res.status === 200) {
    return res.data.items.map(item => ({
      id: item.sys.id,
      title: item.fields.title,
      description: item.fields.description,
      createdby: item.sys.createdBy,
    }));
  }
  return [];
});


export default {
  fetchSpaces,
  fetchSpaceEntries,
  fetchSpaceAssets,
  fetchUsers,
};
