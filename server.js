import express from 'express';
import path from 'path';
import mockService from 'raml-mock-service';
import osprey from 'osprey';
import config from './config';

const parser = require('raml-1-parser');

const server = express();

server.set('view engine', 'ejs');
server.use(express.static('public'));

parser.loadRAML(path.join(__dirname, config.ramlPath), { rejectOnErrors: true })
  .then((ramlApi) => {
    const raml = ramlApi.expand(true).toJSON({ serializeMetadata: false });
    server.use(osprey.server(raml));
    server.use(mockService(raml));
  });

server.get(['/', '/spacexplorer', '/spacexplorer/:spaceID'], (req, res) => {
  res.render('index');
});

server.listen(config.port, config.host, () => {
  console.log('Listening to port number ', config.port);
});
