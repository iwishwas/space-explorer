## Space Explorer.

The Mock API server is integrated with same server where the UI application is deployed.

## Project built on

Mac OS, Node 8, React, Bootstrap, Jest, PropTypes, Babel, WebPack.

## Common setup

Clone the repo and install the dependencies.

```bash
git clone https://iwishwas@bitbucket.org/iwishwas/space-explorer.git
cd space-explorer
```

```bash
npm install
```

## Step to start the project

To start the express server, run the following

```bash
npm start
```

Open [http://localhost:8080](http://localhost:8080) and take a look around.

## Features built

* Users can navigate spaces by clicking on a vertical tab on the left.
* Users can navigate between the entries and assets tab for a given space.
* Users can sort (on the client side) by clicking column headers.
* URL Route should contain a Space GUID.
* Visiting the base URL should redirect to the first space returned by the /spacexplorer end-point.


* Offline state storage. The application works even when server is down.( Based on localStorage ).
* Prop Validation is performed on all the components.
* SnapShot Testing is performed on all the components using Jest.

## Valid Routes

* http://localhost:8080 ,http://localhost:8080/spacexplore
* http://localhost:8080/spacexplorer/yadj1kx9rmg0.
* Current Space GUID will be in url.
* When reloaded any http://localhost:8080/spacexplorer/${spaceID}, It routes you back to http://localhost:8080/spacexplorer/yadj1kx9rmg0.
* All the API routes are valid.

## Design TradeOff

* Making an api call, only when necessary. thus reducing the api calls and saving only important data in the component's state.

## File tree description (src/)

* `App.js` is parent component file where most of the states are saved.
* `AssetList.js` handles rendering of Asset Table in UI.
* `EntryList.js` handles rendering of Entry Table in UI.
* `SpaceContext.js` handles rendering of the right div which contains AssetList and EntryList.
* `SpaceList.js` handles rendering of the left div which contains the vertical space list.
* `api.js` handles calling the apis.
* `index.js` is where the rectDOM is called.
* `__snapshots__` folder for testing related stuffs.
* `__tests__` folder contains the actual test cases written for components.

## Testing and Coverage.

```bash
npm test
```

```bash
npm run test:coverage
```

## Friendly Warning/Error.

* Error: GET /robots.txt does not exist in the RAML for this application.
